Transponder Reminders
===============================

A Starsector mod that prompts the player to turn his transponder on when jumping
into systems with non-hostile patrols and off when jumping into hyperspace.

## Building

To develop from head, clone this into Starsector's `mods` directory and run
`./gradlew build` within the mod directory. This should presently work on Linux
and Windows; OS X would take a slight adaptation of `build.gradle` to point to
the Starsector jars. (Please upstream if you do get that working!)

## Contributing

Pull requests are welcome! Note that I use
[`google-java-format`](https://github.com/google/google-java-format) on Java
source and try to keep JSON files clean (no trailing spaces, no trailing commas,
consistent two-space indentation, spaces between key/value pairs and array
entries).
