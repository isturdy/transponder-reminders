#!/bin/bash

# just so nothing weird happens
set -eu

MOD=$(basename "$(pwd)")
VERSION=$(grep version mod_info.json | grep -oP '\d+(\.\d+)*')
RELEASE=$MOD-$VERSION

# copy over mod to release folder
mkdir -p releases/$MOD
cp -r transponder-reminders* LICENSE changelog.txt README.md mod_info.json src data releases/$MOD

cd releases
zip -r $RELEASE.zip $MOD
rm -rf $MOD
