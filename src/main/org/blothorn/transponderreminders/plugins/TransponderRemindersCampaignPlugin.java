package org.blothorn.transponderreminders.plugins;

import com.fs.starfarer.api.PluginPick;
import com.fs.starfarer.api.campaign.BaseCampaignPlugin;
import com.fs.starfarer.api.campaign.CampaignFleetAPI;
import com.fs.starfarer.api.campaign.InteractionDialogPlugin;
import com.fs.starfarer.api.campaign.JumpPointAPI;
import com.fs.starfarer.api.campaign.SectorEntityToken;
import com.fs.starfarer.api.impl.campaign.JumpPointInteractionDialogPluginImpl;
import com.fs.starfarer.api.impl.campaign.ids.Tags;

public class TransponderRemindersCampaignPlugin extends BaseCampaignPlugin {

  private static final String ID = "TransponderRemindersCampaignPlugin";

  @Override
  public String getId() {
    return ID;
  }

  @Override
  public boolean isTransient() {
    return true;
  }

  @Override
  public PluginPick<InteractionDialogPlugin> pickInteractionDialogPlugin(
      SectorEntityToken interactionTarget) {
    // Make sure we do not catch anything the CoreCampaignPluginImpl would not.
    if (interactionTarget.hasTag(Tags.COMM_RELAY)
        || interactionTarget.getMarket() != null
        || interactionTarget.hasTag(Tags.GATE)
        || interactionTarget.hasTag(Tags.STATION)
        || interactionTarget.hasTag(Tags.HAS_INTERACTION_DIALOG)
        || interactionTarget instanceof CampaignFleetAPI) {
      return null;
    }

    if (interactionTarget instanceof JumpPointAPI) {
      if (interactionTarget
          .getMemoryWithoutUpdate()
          .getBoolean(JumpPointInteractionDialogPluginImpl.UNSTABLE_KEY)) {
        // Avoid interfering with the tutorial.
        return null;
      }
      return new PluginPick<InteractionDialogPlugin>(
          new TransponderReminderJumpPointInteractionDialogPlugin(), PickPriority.MOD_GENERAL);
    }
    return null;
  }
}
