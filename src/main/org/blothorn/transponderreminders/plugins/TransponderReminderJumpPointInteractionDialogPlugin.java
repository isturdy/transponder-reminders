package org.blothorn.transponderreminders.plugins;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.CampaignFleetAPI;
import com.fs.starfarer.api.campaign.FactionAPI;
import com.fs.starfarer.api.campaign.InteractionDialogAPI;
import com.fs.starfarer.api.campaign.InteractionDialogPlugin;
import com.fs.starfarer.api.campaign.JumpPointAPI;
import com.fs.starfarer.api.campaign.JumpPointAPI.JumpDestination;
import com.fs.starfarer.api.campaign.OptionPanelAPI;
import com.fs.starfarer.api.campaign.SectorEntityToken;
import com.fs.starfarer.api.campaign.TextPanelAPI;
import com.fs.starfarer.api.campaign.VisualPanelAPI;
import com.fs.starfarer.api.campaign.econ.MarketAPI;
import com.fs.starfarer.api.campaign.rules.MemoryAPI;
import com.fs.starfarer.api.characters.AbilityPlugin;
import com.fs.starfarer.api.combat.EngagementResultAPI;
import com.fs.starfarer.api.impl.campaign.ids.Abilities;
import com.fs.starfarer.api.impl.campaign.ids.Conditions;
import com.fs.starfarer.api.impl.campaign.ids.Factions;
import com.fs.starfarer.api.loading.Description;
import com.fs.starfarer.api.loading.Description.Type;
import com.fs.starfarer.api.util.Misc;
import java.awt.Color;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.Nullable;
import org.lwjgl.input.Keyboard;

public class TransponderReminderJumpPointInteractionDialogPlugin
    implements InteractionDialogPlugin {

  private static Logger logger =
      Global.getLogger(TransponderReminderJumpPointInteractionDialogPlugin.class);
  boolean promptToDeactivateInHyperspace =
      TransponderRemindersModPlugin.promptToDeactivateInHyperspace;

  private static enum OptionId {
    INIT,
    JUMP_1,
    JUMP_2,
    JUMP_3,
    JUMP_4,
    JUMP_5,
    JUMP_6,
    JUMP_7,
    JUMP_8,
    JUMP_9,
    LEAVE,
  }

  private InteractionDialogAPI dialog;
  private TextPanelAPI textPanel;
  private OptionPanelAPI options;
  private VisualPanelAPI visual;

  private CampaignFleetAPI playerFleet;
  private JumpPointAPI jumpPoint;

  private List<OptionId> jumpOptions =
      Arrays.asList(
          new OptionId[] {
            OptionId.JUMP_1,
            OptionId.JUMP_2,
            OptionId.JUMP_3,
            OptionId.JUMP_4,
            OptionId.JUMP_5,
            OptionId.JUMP_6,
            OptionId.JUMP_7,
            OptionId.JUMP_8,
            OptionId.JUMP_9
          });

  private static final Color HIGHLIGHT_COLOR = Global.getSettings().getColor("buttonShortcut");

  private OptionId lastOptionMousedOver = null;
  private float fuelCost;
  private boolean canAfford;

  private class JumpOptions {
    public JumpDestination destination;
    @Nullable public Boolean transponderSetting;

    public JumpOptions(JumpDestination destination, @Nullable Boolean transponderSetting) {
      this.destination = destination;
      this.transponderSetting = transponderSetting;
    }
  }

  private Map<OptionId, JumpOptions> destinationMap = new HashMap<>();

  public void init(InteractionDialogAPI dialog) {
    this.dialog = dialog;
    textPanel = dialog.getTextPanel();
    options = dialog.getOptionPanel();
    visual = dialog.getVisualPanel();

    playerFleet = Global.getSector().getPlayerFleet();
    jumpPoint = (JumpPointAPI) (dialog.getInteractionTarget());

    fuelCost = playerFleet.getLogistics().getFuelCostPerLightYear();
    float rounded = Math.round(fuelCost);
    if (fuelCost > 0 && rounded <= 0) rounded = 1;
    fuelCost = rounded;
    if (jumpPoint.isInHyperspace()) {
      fuelCost = 0f;
    }
    canAfford = fuelCost <= playerFleet.getCargo().getFuel();

    visual.setVisualFade(0.25f, 0.25f);
    if (jumpPoint.getCustomInteractionDialogImageVisual() != null) {
      visual.showImageVisual(jumpPoint.getCustomInteractionDialogImageVisual());
    } else {
      if (playerFleet.getContainingLocation().isHyperspace()) {
        visual.showImagePortion("illustrations", "jump_point_hyper", 640, 400, 0, 0, 480, 300);
      } else {
        visual.showImagePortion("illustrations", "jump_point_normal", 640, 400, 0, 0, 480, 300);
      }
    }

    dialog.setOptionOnEscape("Leave", OptionId.LEAVE);

    optionSelected(null, OptionId.INIT);
  }

  public Map<String, MemoryAPI> getMemoryMap() {
    return null;
  }

  private EngagementResultAPI lastResult = null;

  public void backFromEngagement(EngagementResultAPI result) {
    // no combat here, so this won't get called
  }

  public void optionSelected(String text, Object optionData) {
    if (optionData == null) return;

    OptionId option = (OptionId) optionData;

    if (text != null) {
      textPanel.addParagraph(text, Global.getSettings().getColor("buttonText"));
    }

    switch (option) {
      case INIT:
        addText(getString("approach"));

        Description desc =
            Global.getSettings().getDescription(jumpPoint.getCustomDescriptionId(), Type.CUSTOM);
        if (desc != null && desc.hasText3()) {
          addText(desc.getText3());
        }

        if (!jumpPoint.isInHyperspace()) {
          if (canAfford) {
            textPanel.addParagraph(
                "Activating this jump point to let your fleet pass through will cost "
                    + (int) fuelCost
                    + " fuel.");
            textPanel.highlightInLastPara(Misc.getHighlightColor(), "" + (int) fuelCost);
          } else {
            int fuel = (int) playerFleet.getCargo().getFuel();
            if (fuel == 0) {
              textPanel.addParagraph(
                  "Activating this jump point to let your fleet pass through will cost "
                      + (int) fuelCost
                      + " fuel. You have no fuel.");
            } else {
              textPanel.addParagraph(
                  "Activating this jump point to let your fleet pass through will cost "
                      + (int) fuelCost
                      + " fuel. You only have "
                      + fuel
                      + " fuel.");
            }
            textPanel.highlightInLastPara(
                Misc.getNegativeHighlightColor(), "" + (int) fuelCost, "" + fuel);
          }
        }
        createInitialOptions();
        break;
      case LEAVE:
        Global.getSector().setPaused(false);
        dialog.dismiss();
        break;
    }

    if (jumpOptions.contains(option)) {
      JumpOptions dest = destinationMap.get(option);
      if (dest != null) {
        dialog.dismiss();

        Global.getSector().setPaused(false);

        if (dest.transponderSetting != null) {
          AbilityPlugin transponderAbility = playerFleet.getAbility(Abilities.TRANSPONDER);
          if (transponderAbility == null) {
            logger.error("Transponder ability not found.");
          } else if (dest.transponderSetting) {
            logger.debug("Enabling transponder.");
            transponderAbility.activate();
          } else {
            logger.debug("Disabling transponder.");
            transponderAbility.deactivate();
          }
        }

        Global.getSector().doHyperspaceTransition(playerFleet, jumpPoint, dest.destination);

        playerFleet.getCargo().removeFuel(fuelCost);

        return;
      }
    }
  }

  private void createInitialOptions() {
    options.clearOptions();

    boolean dev = Global.getSettings().isDevMode();
    float navigation =
        Global.getSector().getPlayerFleet().getCommanderStats().getSkillLevel("navigation");
    boolean isStarAnchor = jumpPoint.isStarAnchor();
    boolean okToUseIfAnchor = isStarAnchor && navigation >= 7;

    okToUseIfAnchor = true;
    if (isStarAnchor && !okToUseIfAnchor && dev) {
      addText("(Can always be used in dev mode)");
    }
    okToUseIfAnchor |= dev;

    boolean transponderOn = playerFleet.isTransponderOn();
    boolean addedTransponderWarning = false;

    if (jumpPoint.getDestinations().isEmpty()) {
      addText(getString("noExits"));
    } else if (playerFleet.getCargo().getFuel() <= 0 && !canAfford) {
      // addText(getString("noFuel"));
    } else if (isStarAnchor && !okToUseIfAnchor) {
      addText(getString("starAnchorUnusable"));
    } else if (canAfford) {
      int index = 0;
      for (JumpDestination dest : jumpPoint.getDestinations()) {
        if (!transponderOn && caresAboutTransponder(dest)) {
          if (index >= jumpOptions.size()) break;
          OptionId option = jumpOptions.get(index);
          index++;

          if (!addedTransponderWarning) {
            addText(
                "Warning: your transponder is off and patrols in some destination systems may require it to be enabled.");
            addedTransponderWarning = true;
          }

          options.addOption(
              "Enable your transponder and order a jump to " + dest.getLabelInInteractionDialog(),
              option,
              null);
          destinationMap.put(option, new JumpOptions(dest, true));
        } else if (promptToDeactivateInHyperspace
            && !playerFleet.isInHyperspace()
            && transponderOn) {
          if (index >= jumpOptions.size()) break;
          OptionId option = jumpOptions.get(index);
          index++;

          options.addOption(
              "Disable your transponder and order a jump to " + dest.getLabelInInteractionDialog(),
              option,
              null);
          destinationMap.put(option, new JumpOptions(dest, false));
        }

        if (index >= jumpOptions.size()) break;
        OptionId option = jumpOptions.get(index);
        index++;

        options.addOption("Order a jump to " + dest.getLabelInInteractionDialog(), option, null);
        destinationMap.put(option, new JumpOptions(dest, null));
      }
    }
    options.addOption("Leave", OptionId.LEAVE, null);
    options.setShortcut(OptionId.LEAVE, Keyboard.KEY_ESCAPE, false, false, false, true);

    if (Global.getSettings().getBoolean("oneClickJumpPoints")) {
      if (jumpPoint.getDestinations().size() == 1) {
        dialog.setOpacity(0);
        dialog.setBackgroundDimAmount(0f);
        optionSelected(null, OptionId.JUMP_1);
      }
    }
  }

  public void optionMousedOver(String optionText, Object optionData) {}

  public void advance(float amount) {}

  private void addText(String text) {
    textPanel.addParagraph(text);
  }

  private void appendText(String text) {
    textPanel.appendToLastParagraph(" " + text);
  }

  private String getString(String id) {
    String str = Global.getSettings().getString("jumpPointInteractionDialog", id);

    String fleetOrShip = "fleet";
    if (playerFleet.getFleetData().getMembersListCopy().size() == 1) {
      fleetOrShip = "ship";
      if (playerFleet.getFleetData().getMembersListCopy().get(0).isFighterWing()) {
        fleetOrShip = "fighter wing";
      }
    }
    str = str.replaceAll("\\$fleetOrShip", fleetOrShip);

    return str;
  }

  public Object getContext() {
    return null;
  }

  private boolean caresAboutTransponder(JumpDestination destination) {
    if (!jumpPoint.isInHyperspace()) {
      return false;
    }
    boolean cares = false;
    for (SectorEntityToken entity :
        destination.getDestination().getContainingLocation().getAllEntities()) {
      MarketAPI market = entity.getMarket();
      if (market == null) {
        continue;
      }
      final FactionAPI faction = market.getFaction();
      cares |=
          !(market.hasCondition(Conditions.FREE_PORT)
              || faction.getCustomBoolean(Factions.CUSTOM_ALLOWS_TRANSPONDER_OFF_TRADE)
              || faction.getCustomBoolean(Factions.CUSTOM_ENGAGES_IN_HOSTILITIES)
              || faction.getRelToPlayer().isHostile());
    }
    return cares;
  }
}
