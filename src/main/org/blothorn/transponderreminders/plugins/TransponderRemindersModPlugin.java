package org.blothorn.transponderreminders.plugins;

import com.fs.starfarer.api.BaseModPlugin;
import com.fs.starfarer.api.Global;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.json.JSONObject;

public class TransponderRemindersModPlugin extends BaseModPlugin {

  private static Logger logger = Global.getLogger(TransponderRemindersModPlugin.class);

  private static final String SETTINGS_FILE = "transponder-reminders_settings.json";

  static boolean promptToDeactivateInHyperspace = false;

  @Override
  public void onApplicationLoad() throws Exception {
    JSONObject settings = Global.getSettings().loadJSON(SETTINGS_FILE);
    String logLevelString = settings.optString("logLevel");
    if (logLevelString == null) {
      logger.error("Transponder Reminders setting \"logLevel\" not found.");
      logLevelString = "ERROR";
    }
    Level logLevel = Level.toLevel(logLevelString);
    if (logLevel == null) {
      logger.error("Transponder Reminders setting \"logLevel\" unparseable.");
      logLevel = Level.ERROR;
    }
    setLogLevel(logLevel);

    promptToDeactivateInHyperspace = settings.optBoolean("promptToDeactivateInHyperspace");
    logger.info("promptToDeactivateInHyperspace: " + promptToDeactivateInHyperspace);
  }

  @Override
  public void onGameLoad(boolean newGame) {
    Global.getSector().registerPlugin(new TransponderRemindersCampaignPlugin());
  }

  private static void setLogLevel(Level level) {
    Global.getLogger(TransponderRemindersCampaignPlugin.class).setLevel(level);
    Global.getLogger(TransponderReminderJumpPointInteractionDialogPlugin.class).setLevel(level);
    Global.getLogger(TransponderRemindersModPlugin.class).setLevel(level);
  }
}
